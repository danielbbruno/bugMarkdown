# Example - Bug in markdown
For [this](https://gitlab.com/gitlab-org/gitlab-ce/issues/41395) issue.

## Without newline
1. This code will be incorrectly rendered
```shell
git status
```
2. Same here
```shell
git status
```
3. Same here
```shell
git status
```

## With newline
1. This code will be correctly rendered
```shell
git status
```

2. Same here
```shell
git status
```
3. Same here
```shell
git status
```
